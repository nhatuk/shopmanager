package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class OrderTableController {

    @GetMapping("/table/order")
    public String order(){
        return "table_order/datatables";
    }
}
